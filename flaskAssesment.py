from pymongo import MongoClient
import json
Cluster=MongoClient('mongodb://127.0.0.1:27017/')
Test=Cluster['test']
Collections=Test['posts']
from flask import Flask,jsonify,request
app = Flask(__name__)
@app.route('/',methods=['GET','POST'])
def myFirstAPI():
    if request.method=="POST":
        reqJson=request.json
        print(reqJson["name"])
        Object=list((Collections.find({"Name":reqJson["name"]},{"_id":0})))
        if(len(Object)==0):
            return(jsonify("Sorry No Users Found"))
        Object=(str(Object)[1:-1]).replace("'",'"')
        return jsonify(Object)       
    else:
        return jsonify("Null")    

if __name__=="__main__":
    app.run(debug=True)